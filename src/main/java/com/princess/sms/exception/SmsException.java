package com.princess.sms.exception;

import com.princess.sms.ExceptionCode;


/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * 
 * @version $Revision: 1.0 $
 */
public class SmsException extends RuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = -7538597821090026714L;
  private final ExceptionCode code;
  private final String message;

  /**
   * 
   * @param code
   */
  public SmsException(ExceptionCode code) {
    super(code.getDescription());
    this.message = null;
    this.code = code;
  }

  /**
   * 
   * @param code
   * @param message String
   */
  public SmsException(ExceptionCode code, String message) {
    super(code.getDescription());
    this.code = code;
    this.message = message;
  }

  /**
   * 
   * @param code
   * @param cause
   */
  public SmsException(ExceptionCode code, Throwable cause) {
    super(code.getDescription(), cause);
    this.code = code;
    this.message = null;
  }

  /**
   * 
   * @param cause
   */
  public SmsException(Throwable cause) {
    super(cause);
    this.code = null;
    this.message = null;
  }


  /**
   * Method getCode.
   * 
   * 
   * @return ExceptionCode
   */
  public ExceptionCode getCode() {
    return code;
  }

  /**
   * Method getMessage.
   * 
   * 
   * @return String
   */
  @Override
  public String getMessage() {
    return message;
  }

}
