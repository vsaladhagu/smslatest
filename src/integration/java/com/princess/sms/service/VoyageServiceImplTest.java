package com.princess.sms.service;


import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.princess.sms.model.Voyage;


public class VoyageServiceImplTest extends AbstractServiceIntegrationTestCase {

  @Autowired
  VoyageService voyageService;

  @Test
  public void testFindCurrentVoyage() {
    Voyage voyage = voyageService.findCurrentVoyage();
    Assert.assertEquals("H446", voyage.getVoyageNumber());
    Assert.assertNotNull(voyage);
  }
}
