package com.princess.sms.service;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.princess.sms.Application;


/**
 * Base class for running Dao tests.
 * 
 * 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Ignore
public abstract class AbstractServiceIntegrationTestCase extends AbstractJUnit4SpringContextTests {

  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractServiceIntegrationTestCase.class);



}
